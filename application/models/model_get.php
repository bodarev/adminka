<?php
	
	class Model_get extends CI_Model {
	
	   function getData($namePage){
           
            if ($this->session->userdata('langue')) {
				$lang = $this->session->userdata('langue');
			} else {
				$lang = 'Ro';
			}
			$data = 'data'.$lang;
           
            $this->db->select("key, $data as denumire");
			$this->db->from("pagedata");		
			$this->db->where('namePage =', $namePage);	

            $this->db->or_where('namePage =', 'nav');
			$this->db->or_where('namePage =', 'admin_nav');
           
            $this->db->or_where('namePage =', 'months');
            $this->db->or_where('namePage =', 'order');
            $this->db->or_where('namePage =', 'status');
           
            $this->db->or_where('namePage =', 'product');
           	$this->db->or_where('namePage =', 'prod_categ');
            $this->db->or_where('namePage =', 'currency');
           	
            $this->db->or_where('namePage =', 'msg');
			$this->db->or_where('namePage =', 'user');
			$this->db->or_where('namePage =', 'button');
			$this->db->or_where('namePage =', 'link');
			$this->db->or_where('namePage =', 'validation');
	
 			$query = $this->db->get();
	
			$data = array();
			foreach ($query->result() as $row)
			{
				foreach ($row as $k=>$value) {   
					if ($k == 'key') {
						$campo = $value;
					} else {
						$data[$campo] = $value;		
					}
				}
			}
			
			$data['current'] = $namePage;
			$index = $namePage."_title";
			$data['tab_title'] = $data[$index];
			
			return $data;			
		}	
	}

?>