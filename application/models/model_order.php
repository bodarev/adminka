<?php
	
	class Model_order extends CI_Model {
		
		function get_count_from_orders(){
			$count = $this->db->count_all('orders_total');	
			
			return $count;			
		}
        
        function get_orders(){		
            
            $this->db->select('*, users.id as user');
            $this->db->from('orders_total');
            $this->db->join('users', 'orders_total.email = users.email');
       		
 			$query = $this->db->get();

			return $query->result();
		}
        
        function process_order($id){		
			
            $data = array(
               'status' => 'processed'
            );
        
            $this->db->where('id_order', $id);
            $this->db->update('orders_total', $data); 			
		}
        
        function cancel_order($id){		
			
            $data = array(
               'status' => 'canceled'
            );
        
            $this->db->where('id_order', $id);
            $this->db->update('orders_total', $data); 			
		}
        
        function getStatus($key){
            
            if ($this->session->userdata('langue')) {
                    $lang = $this->session->userdata('langue');
                } else {
                    $lang = 'Ro';
                }
                $data = 'data'.$lang;

                $this->db->select("$data as denumire");
                $this->db->from("pagedata");		
                $this->db->where('namePage =', 'status');
                $this->db->where('key =', $key);
            
                $query = $this->db->get();
            
                return $query->row('denumire');

		
            }	
        
	}

?>