<?php
	
	class Model_product extends CI_Model {
		
		function getProduct($categ, $ip){		
			
			$this->db->select('*');
			$this->db->from('prod');
			$this->db->join('det_prod', 'prod.id = det_prod.id_prod');
			if ($ip > '') {
				$this->db->join('cart', 'prod.id = cart.id_prod');
				$this->db->where('cart.id_client', $ip);
			}
			if  ($categ > 0) {
				$this->db->where('prod.categ_prod', $categ );	
			}
			switch ($this->session->userdata('langue')) {
    		case 'Ro':
				$this->db->where('det_prod.lang_prod', 'Ro');	
       		break;
    		case 'It':
				$this->db->where('det_prod.lang_prod', 'It');	
        	break;
    		case 'Ru':
				$this->db->where('det_prod.lang_prod', 'Ru');;	
        	break;
			default : 
				$this->db->where('det_prod.lang_prod', 'Ro');  
			}	 			
 			$query = $this->db->get();
			
			return $query->result();			
		}
		
		function getProductId($id){
				
			$this->db->select('*');
            $this->db->where('prod.id', $id);
			$this->db->from('prod');
			$this->db->join('det_prod', 'prod.id = det_prod.id_prod');
            $this->db->order_by("det_prod.id", "asc");
            
 			$query = $this->db->get();
          
			return $query->result();			
		}
		
		function insertProduct($titleRo, $titleIt, $titleRu, $textRo, $textIt, $textRu, $category, $price){	
		
		
		$time = time();
		
		
		/* Incarcarea fisierului pe server */
			
			$config['upload_path'] = './prod_img/photos/';
			$config['allowed_types'] = 'gif|jpg|png|gpeg';
			$config['max_size']	= '10240';
			$config['encrypt_name']	= TRUE;
			$config['remove_spaces'] = TRUE;
			
			$this->load->library('upload', $config);
			
			$this->upload->do_upload();
			
			$data_img = $this->upload->data();
			
		/* Crearea thumb-ului */
			
			$config = array (
				'image_library' => 'gd2',
				'source_image' => $data_img['full_path'],
				'new_image' => APPPATH.'../prod_img/thumb/'.$time.'b13_thumb.png',
				'create_thumb' => FALSE,
				'maintain_ratio' => FALSE,
				'width' => 250,
				'height' => 200
			);
				
			$this->load->library('image_lib', $config);
			
			$this->image_lib->resize();
			
		/* Crearea unei imagini cu logotip */
			
			$config['source_image']	= $data_img['full_path'];
			$config['new_image'] = APPPATH.'../prod_img/img/'.$time.'b13.png';
			$config['create_thumb'] = FALSE;
		    $config['maintain_ratio'] = FALSE;
			$config['width'] = 1600;
			$config['height'] = 1000;
		
			// pentru logo
			//$config['wm_type'] = 'overlay';
			//$config['wm_overlay_path'] = './prod_img/logo.png';
			//$config['wm_vrt_alignment'] = 'bottom';
			//$config['wm_hor_alignment'] = 'right';
			//$config['wm_hor_offset'] = '100';
			//$config['wm_vrt_offset'] = '100';
			//$config['wm_opacity'] = '50';
			$this->image_lib->initialize($config); 
			//$this->image_lib->watermark();
			
			//$this->load->library('image_lib', $config);
			
			$this->image_lib->resize();
			
			$thumb_img_prod = 'prod_img/thumb/'.$time.'b13_thumb.png';
			$img_prod = 'prod_img/img/'.$time.'b13.png';
		
		/* Inseram in tabela prod */ 
		
			$product = array(
               'pret_prod' => $price,
               'categ_prod' => $category,
               'orig_img_prod' => $img_prod,
			   'thumb_img_prod' => $thumb_img_prod
            );
			$this->db->insert('prod', $product);
			
		/* Stergem fisierul sursa	*/
		
			unlink($data_img['full_path']);
		
		/* Depistam id-ul maxim (adica ultimul inserat)	*/
			
			$this->db->select_max('id');
			$this->db->from("prod");	
 			$query = $this->db->get();
			$row = $query->row(); 
			$max_id = $row->id;
			
		/* Inseram in det_prod - detaliile */ 	
			
			$det_product_ro = array(
               'id_prod' => $max_id,
               'title_prod' => $titleRo,
               'desc_prod' => $textRo,
			   'lang_prod' => 'Ro'
            );
			$this->db->insert('det_prod', $det_product_ro);
			
			$det_product_it = array(
               'id_prod' => $max_id,
               'title_prod' => $titleIt,
               'desc_prod' => $textIt,
			   'lang_prod' => 'It'
            );
			$this->db->insert('det_prod', $det_product_it); 
			
			$det_product_ru = array(
               'id_prod' => $max_id,
               'title_prod' => $titleRu,
               'desc_prod' => $textRu,
			   'lang_prod' => 'Ru'
            );
			$this->db->insert('det_prod', $det_product_ru);  
			 			
			return true;			
		}
        
        function updateProduct($id, $titleRo, $titleIt, $titleRu, $textRo, $textIt, $textRu, $category, $price){	
		
        if (isset($_FILES['userfile']) && is_uploaded_file($_FILES['userfile']['tmp_name'])) {    
		
            $time = time();


            /* Incarcarea fisierului pe server */

                $config['upload_path'] = './prod_img/photos/';
                $config['allowed_types'] = 'gif|jpg|png|gpeg';
                $config['max_size']	= '10240';
                $config['encrypt_name']	= TRUE;
                $config['remove_spaces'] = TRUE;

                $this->load->library('upload', $config);

                $this->upload->do_upload();

                $data_img = $this->upload->data();

            /* Crearea thumb-ului */

                $config = array (
                    'image_library' => 'gd2',
                    'source_image' => $data_img['full_path'],
                    'new_image' => APPPATH.'../prod_img/thumb/'.$time.'b13_thumb.png',
                    'create_thumb' => FALSE,
                    'maintain_ratio' => FALSE,
                    'width' => 250,
                    'height' => 200
                );

                $this->load->library('image_lib', $config);

                $this->image_lib->resize();

            /* Crearea unei imagini cu logotip */

                $config['source_image']	= $data_img['full_path'];
                $config['new_image'] = APPPATH.'../prod_img/img/'.$time.'b13.png';
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = FALSE;
                $config['width'] = 1600;
                $config['height'] = 1000;

                // pentru logo
                //$config['wm_type'] = 'overlay';
                //$config['wm_overlay_path'] = './prod_img/logo.png';
                //$config['wm_vrt_alignment'] = 'bottom';
                //$config['wm_hor_alignment'] = 'right';
                //$config['wm_hor_offset'] = '100';
                //$config['wm_vrt_offset'] = '100';
                //$config['wm_opacity'] = '50';
                $this->image_lib->initialize($config); 
                //$this->image_lib->watermark();

                //$this->load->library('image_lib', $config);

                $this->image_lib->resize();

                $thumb_img_prod = 'prod_img/thumb/'.$time.'b13_thumb.png';
                $img_prod = 'prod_img/img/'.$time.'b13.png';

            /* Stergem fisierele existente pentruca incarcam altele */    

                $this->db->select('*');
                $this->db->from('prod');
                $this->db->where('id', $id);
                $query = $this->db->get();
                $row = $query->row(); 
                $img = $row->orig_img_prod;
                $thumb = $row->thumb_img_prod;	

                unlink($img);
                unlink($thumb);    

            /* Editam in tabela prod */ 

                $product = array(
                   'pret_prod' => $price,
                   'categ_prod' => $category,
                   'orig_img_prod' => $img_prod,
                   'thumb_img_prod' => $thumb_img_prod
                );
                $this->db->where('id', $id);
                $this->db->update('prod', $product);

            /* Stergem fisierul sursa	*/

                unlink($data_img['full_path']);
        } else {
             /* Editam in tabela prod dar fara imagine */ 

                $product = array(
                   'pret_prod' => $price,
                   'categ_prod' => $category
                );
                $this->db->where('id', $id);
                $this->db->update('prod', $product);
            
        }
		
			
		/* Editam in det_prod - detaliile */ 	
			
			$det_product_ro = array(
               'title_prod' => $titleRo,
               'desc_prod' => $textRo
            );
            $this->db->where('id_prod', $id);
            $this->db->where('lang_prod', 'Ro');
			$this->db->update('det_prod', $det_product_ro);
			
			$det_product_it = array(
               'title_prod' => $titleIt,
               'desc_prod' => $textIt
            );
            $this->db->where('id_prod', $id);
            $this->db->where('lang_prod', 'It');            
			$this->db->update('det_prod', $det_product_it); 
			
			$det_product_ru = array(
               'title_prod' => $titleRu,
               'desc_prod' => $textRu
            );
            $this->db->where('id_prod', $id);
            $this->db->where('lang_prod', 'Ru');            
			$this->db->update('det_prod', $det_product_ru);  
			 			
			return true;			
		}
		
		function deleteProduct($id){
			
			$this->db->select('*');
			$this->db->from('prod');
			$this->db->where('id', $id);
			$query = $this->db->get();
			$row = $query->row(); 
			$img = $row->orig_img_prod;
			$thumb = $row->thumb_img_prod;
				
			$this->db->where('id', $id);
			$this->db->delete('prod');
			$this->db->where('id_prod', $id);
			$this->db->delete('det_prod');
			$this->db->where('id_prod', $id);
			$this->db->delete('cart');	

			unlink($img);
			unlink($thumb);
								
			return true;			
		}
		
		function getGroup(){
				
			$this->db->select('*');
			$this->db->from('group');
            $this->db->order_by('name', 'desc');
            
 			$query = $this->db->get();
          
			return $query->result();			
		}     
		
		function insertGroup($titleRo, $titleIt, $titleRu, $group){
			
			$pagedata = array(
               'namePage' => 'prod_categ',
               'key'    => $group,
               'dataRo' => $titleRo,
			   'dataIt' => $titleIt,
			   'dataRu' => $titleRu
            );
			
			$this->db->insert('pagedata', $pagedata);
			
			$data = array(
               'name' => $group
            );
			
			$this->db->insert('group', $data); 	
		}
		
		function deleteGroup($id){
			
			$this->db->select('*');
			$this->db->from('group');
			$this->db->where('id', $id);
			$query = $this->db->get();
			$row = $query->row(); 
			$name = $row->name;
				
			$this->db->where('id', $id);
			$this->db->delete('group');
			$this->db->where('key', $name);
			$this->db->delete('pagedata');
								
			return true;			
		}
		
		function getCategory(){
				
			$this->db->select('*');
			$this->db->from('category');
            $this->db->order_by('name_group', 'desc');
            
 			$query = $this->db->get();
          
			return $query->result();			
		}
        
        function insertCategory($titlecRo, $titlecIt, $titlecRu, $category, $group){
			
			$pagedata = array(
               'namePage' => 'prod_categ',
               'key'    => $category,
               'dataRo' => $titlecRo,
			   'dataIt' => $titlecIt,
			   'dataRu' => $titlecRu
            );
			
			$this->db->insert('pagedata', $pagedata);
            
            $this->db->select('*');
			$this->db->from('group');
			$this->db->where('id', $group);
			$query = $this->db->get();
			$row = $query->row(); 
			$name_group = $row->name;
			
			$data = array(
                'icon_category'  => 'fa-test',
                'name_category' => $category,
                'name_group'    => $name_group
            );
			
			$this->db->insert('category', $data); 	
		}
        
		function deleteCategory($id){
			
			$this->db->select('*');
			$this->db->from('category');
			$this->db->where('id', $id);
			$query = $this->db->get();
			$row = $query->row(); 
			$name = $row->name_category;
				
			$this->db->where('id', $id);
			$this->db->delete('category');
			$this->db->where('key', $name);
			$this->db->delete('pagedata');
								
			return true;			
		}
		        
        
	}

?>