<?php
	class Model_users extends CI_Model {
	
		public function can_log_in() {
			$this->db->where('email', $this->input->post('email'));
			$this->db->where('password', md5($this->input->post('password')));
			
			$query = $this->db->get('users_admin');
			
			if ($query->num_rows() == 1) {
				return true;
			} else {
				return false;
			}
		}
		public function getName() {
			$this->db->select();
			$this->db->from("users_admin");
			$this->db->where('email', $this->input->post('email'));
			$this->db->where('password', $this->input->post('password'));	
			$query = $this->db->get();
			
			return $query->result_array();
			
		}
        
        function get_count_from_users(){
			$count = $this->db->count_all('users');	
			
			return $count;			
		}
        
        function get_user_data($id){		
			
            $this->db->select('*');
			$this->db->from('users');
			$this->db->where('users.id', $id);
            $this->db->where('users_address.priority', '1');
            $this->db->join('users_address', 'users.email = users_address.email');

 			$query = $this->db->get();
			
			return $query->row();			
		}
	}
?>
