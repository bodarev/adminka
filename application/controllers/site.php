<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {


	
	public function index()
	{	
		$this->charts();
	}
    
    public function charts() {
        if ($this->session->userdata('is_logged_in')) {
            $this->load->model("model_get");
            $data = $this->model_get->getData("charts");
            $this->load->model("model_order");
            $data['orders_count'] = $this->model_order->get_count_from_orders();
            $this->load->model("model_users");
            $data['users_count'] = $this->model_users->get_count_from_users();
			$this->load->view('charts', $data);
		}else {
			$this->login();
		}	     
    }
    
    //-------------------- Order -------------------- \\
	
    public function processOrder() {
        $id = $this->input->post("valore");
        $this->load->model("model_order");
        $this->model_order->process_order($id);
        $new_status = $this->model_order->getStatus('processed');
        echo $new_status;	
	}
    
    public function cancelOrder() {
        $id = $this->input->post("valore");
        $this->load->model("model_order");
        $this->model_order->cancel_order($id);
        $new_status = $this->model_order->getStatus('canceled');
        echo $new_status;	
	}
    
    
    //-------------------- User -------------------- \\
    
    public function user($id) {
        $this->load->model("model_get");
		$data = $this->model_get->getData("user");
        
        $this->load->model("model_users");
        $data['userData'] = $this->model_users->get_user_data($id);
		$this->load->view('user', $data);	
	}
    
    
    //-------------------- Product -------------------- \\
    
	public function product() {
        $this->load->model("model_get");
		$data = $this->model_get->getData("products");
		$this->load->model("model_product");
		$data['result'] = $this->model_product->getProduct(0, '');
		$this->load->view('product', $data);	
	}
	
	public function addProduct() {
		$this->load->model("model_get");
		$data = $this->model_get->getData("insertProduct");
        $this->load->model("model_product");
        $data['category'] = $this->model_product->getCategory();
		$this->load->view('addProduct', $data);
	}	
	
	public function editProduct($id='') {
		$this->load->model("model_get");
		$data = $this->model_get->getData("editProduct");
		$this->load->model("model_product");
		$data['query'] = $this->model_product->getProductId($id);
        $data['category'] = $this->model_product->getCategory();
		$this->load->view('editProduct', $data);
	}
    
	public function insertProduct() {
		$titleRo = $this->input->post("titleRo");
		$titleIt = $this->input->post("titleIt");
		$titleRu = $this->input->post("titleRu");
		$textRo = $this->input->post("textRo");
		$textIt = $this->input->post("textIt");
		$textRu = $this->input->post("textRu");
		$category = $this->input->post("category");
		$price = $this->input->post("price");
		
		$this->load->model("model_product");
		$this->model_product->insertProduct($titleRo, $titleIt, $titleRu, $textRo, $textIt, $textRu, $category, $price);
		redirect("site/product");
	}
	
    public function updateProduct($id) {
		$titleRo = $this->input->post("titleRo");
		$titleIt = $this->input->post("titleIt");
		$titleRu = $this->input->post("titleRu");
		$textRo = $this->input->post("textRo");
		$textIt = $this->input->post("textIt");
		$textRu = $this->input->post("textRu");
		$category = $this->input->post("category");
		$price = $this->input->post("price");
		
		$this->load->model("model_product");
		$this->model_product->updateProduct($id, $titleRo, $titleIt, $titleRu, $textRo, $textIt, $textRu, $category, $price);
		redirect("site/product");
	}
    
	public function deleteProduct($id='') {
		$this->load->model("model_product");
		$data['query'] = $this->model_product->deleteProduct($id);
		redirect("site/product");
	} 
	
	//---------------   Group/Category   --------------- \\
	
	public function managerCategory() {
		$this->load->model("model_get");
		$data = $this->model_get->getData("managerCategory");
		$this->load->model("model_product");
        $data['group'] = $this->model_product->getGroup();
		$data['category'] = $this->model_product->getCategory();
		$this->load->view('managerCategory', $data);
	}	
	
	public function addGroup() {
		$titleRo = $this->input->post("titleRo");
		$titleIt = $this->input->post("titleIt");
		$titleRu = $this->input->post("titleRu");
		$group = $this->input->post("group");
		
		$this->load->model("model_product");
		$this->model_product->insertGroup($titleRo, $titleIt, $titleRu, $group);
        redirect("site/managerCategory");
	}
	
	public function deleteGroup($id='') {
		$this->load->model("model_product");
		$data['query'] = $this->model_product->deleteGroup($id);
        redirect("site/managerCategory");
	}
    
    public function addCategory() {
		$titlecRo = $this->input->post("titlecRo");
		$titlecIt = $this->input->post("titlecIt");
		$titlecRu = $this->input->post("titlecRu");
		$category = $this->input->post("category");
        $group = $this->input->post("group");
		
		$this->load->model("model_product");
		$this->model_product->insertCategory($titlecRo, $titlecIt, $titlecRu, $category, $group);
        redirect("site/managerCategory");
	}
    
    public function deleteCategory($id='') {
		$this->load->model("model_product");
		$data['query'] = $this->model_product->deleteCategory($id);
        redirect("site/managerCategory");
	}
    
    //--------------------   Orders   -------------------- \\
    
    public function allOrders() {
        if ($this->session->userdata('is_logged_in')) {
            $this->load->model("model_get");
            $data = $this->model_get->getData("allOrders");
            $this->load->model("model_order");
            $data['result'] = $this->model_order->get_orders();
            $this->load->view('allOrders', $data);
		}else {
			$this->login();
		}	     
    }
    
    //--------------------   Login   -------------------- \\
 	

	public function login() {
        $this->load->model("model_get");
		$data = $this->model_get->getData("managerCategory");
		$this->load->view('login', $data);
	}
	public function login_validation() {	
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback_validate_credentials|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|MD5|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="alert bg-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> ', 
		'<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a></div>');
		
		$this->form_validation->set_message('required', 'Câmpul %s este obligator!');
		$this->form_validation->set_message('valid_email', 'Introduceți un email valid!');
		
		if ($this->form_validation->run()) {
			$this->load->model('model_users');
			
			$query['test'] = $this->model_users->getName();
			$xx = $query['test'][0];

			$name = $xx['name'];
		
			$data =  array (	
				'email' => $this->input->post('email'),
				'name' => $name,
				'is_logged_in' => 1
			);
			$this->session->set_userdata($data);  	  
			redirect('site/charts');
		} else {
			$this->load->view('login');
		}
	}
	
	public function validate_credentials() {	
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		if(!empty($email) && !empty($password))
		{
			$this->load->model('model_users');
			
			if($this->model_users->can_log_in()) {
				return true;
			} else {
				$this->form_validation->set_message('validate_credentials', 'Nu exista asa combinatie de mail/password!');
				return false;
			}
		} else {
			$this->form_validation->set_message('validate_credentials', '');
			return false;
		}
	}
	
	public function logout() {
		if ($this->session->userdata('langue')) {$lang = $this->session->userdata('langue');  } else { $lang = 'Ro'; } 
		$link = uri_string();
		$this->session->sess_destroy();
		redirect('site/langue/'.$lang.'/'.$link);
		redirect('site/login');
	}

     //--------------------   Langue   -------------------- \\
    
	public function langue($lang='',$link='') {
			$data = array (
				'langue' => $lang
			);
			$this->session->set_userdata($data);
			
			if($link == 'null') { 
				redirect('/');
			} else {
				$link =  str_replace("-","/", $link);
				redirect($link);
            } 
	}
}	