<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>

<body>
    <?php include 'navbar.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active">
                    <?php echo $tab_title; ?>
                </li>
            </ol>
        </div>
        <!--/.row-->

        <?php  if ($userData == null ) { ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $MsgUserNotFound; ?>
                    </div>
                </div>
            </div>
        </div>

        <?php } else {  ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $tab_title.' '.$userData->name; ?>
                    </div>
                    <div class="panel-body">
                        <?php

                        $day = date("j ", $userData->register_date);
                        $month = date("F", $userData->register_date);
                        $year = date(" Y H:i", $userData->register_date);
                        $date = $day.${$month}.$year;

                        if ($userData->avatar > null ) {
                            $avatar = $userData->avatar;
                        } else {
                            $avatar = "prod_img/default.jpg";   
                        }

                        echo '<br />

                            <section>
                                <article class="personal-data">
                                    <div class="col-sm-3 personal-image">
                                         <img src="'.base_url().$avatar.'" class="img-thumbnail" width="580" >
                                    </div>

                                    <div class="col-sm-9">
                                         <p> <svg class="glyph stroked location pin"><use xlink:href="#stroked-location-pin"/></svg>'.$userData->strada.',&nbsp'
                                            .$userData->bloc;

                                            if ($userData->scara > 0) {	
                                               echo ',&nbsp'.$user_entrance.'&nbsp'
                                              .$userData->scara;
                                            }
                                            if ($userData->etaj > 0) {
                                               echo ',&nbsp'.$user_floor.'&nbsp'
                                              .$userData->etaj;
                                            }
                                            if ($userData->apartament > 0) {
                                               echo ',&nbsp'.$user_apartament_short.'&nbsp'
                                              .$userData->apartament;
                                            }
                                            echo '<br />
                                            <svg class="glyph stroked home"><use xlink:href="#stroked-home"/></svg>'.$userData->localitate.'
                                            <br />
                                            <svg class="glyph stroked mobile device"><use xlink:href="#stroked-mobile-device"/></svg>'.$user_phone.':&nbsp'.$userData->phone.'
                                            <br />
                                            <svg class="glyph stroked email"><use xlink:href="#stroked-email"/></svg>E-mail:&nbsp'.$userData->email.'
                                            <br />
                                            <svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"/></svg>'.$user_regdate.':&nbsp'.$date.'  
                                        </p>
                                    </div>
                                </article>
                            </section>'; 
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <!--.row-->

    </div>
    <!--/.main-->


    <script src="<?php echo base_url();?>/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>/js/chart.min.js"></script>
    <script src="<?php echo base_url();?>/js/easypiechart.js"></script>
    <script src="<?php echo base_url();?>/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>/js/bootstrap-table.js"></script>
    <script src="<?php echo base_url();?>/js/lumino.glyphs.js"></script>

</body>

</html>