<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
    <body>
        <?php include 'navbar.php'; ?>
        <?php include 'sidebar.php'; ?>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="#"><svg class="glyph stroked home"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-home"></use></svg></a></li>
                    <li class="active">
                        <?php echo $tab_title; ?>
                    </li>
                </ol>
            </div><!--/.row-->
	
            <?php foreach($query as $row) { 
                if ($row->lang_prod == 'Ro') {
                    $titleRo = $row->title_prod;
                    $descRo = $row->desc_prod;
                } else if ($row->lang_prod == 'It') {
                    $titleIt = $row->title_prod;
                    $descIt = $row->desc_prod;
                } else {
                    $titleRu = $row->title_prod;
                    $descRu = $row->desc_prod;
                }
            }?><!--/.foreach-->
		
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form role="form" action="<?php echo base_url();?>site/updateProduct/<?php echo $row->id_prod; ?>" method="post" enctype="multipart/form-data">
                                <div class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab1" data-toggle="tab">Română</a></li>
                                        <li><a href="#tab2" data-toggle="tab">Italiano</a></li>
                                        <li><a href="#tab3" data-toggle="tab">Русский</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tab1">
                                            <div class="form-group">
                                                <br />
                                                <label for="titleRo"><?php echo $productInsertTitle;?>:</label>
                                                <input class="form-control" placeholder="Title" value="<?php echo $titleRo;?>" name="titleRo" id="titleRo">
                                                <br />
                                                <label for="descRo">I<?php echo $productInsertDescription;?>:</label>
                                                <textarea class="form-control" rows="7" name="textRo" id="textRo"><?php echo $descRo;?> </textarea>
                                            </div>
                                        </div>

                                        <div class="tab-pane fade" id="tab2">
                                            <div class="form-group">
                                                <br />
                                                <label for="titleIt"><?php echo $productInsertTitle;?>:</label>
                                                <input class="form-control" placeholder="Title" value="<?php echo $titleIt;?>" name="titleIt" id="titleIt">
                                                <br />
                                                <label for="textIt"><?php echo $productInsertDescription;?>:</label>
                                                <textarea class="form-control" rows="7" name="textIt" id="textIt"><?php echo $descIt;?> </textarea>
                                            </div>
                                        </div>

                                        <div class="tab-pane fade" id="tab3">
                                            <div class="form-group">
                                                <br />
                                                <label for="textRu"><?php echo $productInsertTitle;?>:</label>
                                                <input class="form-control" placeholder="Title" value="<?php echo $titleRu;?>" name="titleRu" id="titleRu">
                                                <br />
                                                <label for="textRu"><?php echo $productInsertDescription;?>:</label>
                                                <textarea class="form-control" rows="7" name="textRu" id="textRu"><?php echo $descRu;?> </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="category"><?php echo $productSelectCategory;?>:</label>
                                    <select class="form-control" id="sel" name="category">

                                        <?php foreach($category as $rowcat) { 

                                            if($rowcat->id == $row->categ_prod) { ?>
                                                <option value="<?php echo $rowcat->id; ?>" selected>
                                                    <?php echo "${$rowcat->name_category}"; ?>
                                                </option>                                              
                                            <?php } else { ?>
                                                <option value="<?php echo $rowcat->id; ?>">
                                                    <?php echo "${$rowcat->name_category}"; ?>
                                                </option>
                                            <?php }

                                         }?>

                                    </select>
                                </div>
                                                
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="range">
                                            <label for="price"><?php echo $productInsertPrice;?>:</label>
                                            <input type='range' id='r1' class='tip fill fill-replace' value='<?php echo $row->pret_prod;?>' min='0' max='999' name="price" />
                                            <output for="range" class="output"><?php echo $row->pret_prod;?>,00</output>
                                        </div>
                                    </div> <!--/.col-->

                                    <div class="col-md-2">
                                        <label for="category"><?php echo $productUploadImage;?>:</label>
                                        <br />
                                        <div class="fileUpload btn btn-primary">
                                            <span class="glyphicon glyphicon-inbox" aria-hidden="true"></span>
                                            <span><?php echo $productUploadImage;?></span>
                                            <input type="file" name="userfile" id="uploadImage" class="upload" accept="image/*">
                                        </div>
                                    </div><!--/.col-->

                                    <div class="col-md-4">
                                        <img src="<?php echo base_url();?><?php echo $row->thumb_img_prod;?>" id="preview" alt="" width="128" height="104" class="img-thumbnail" />
                                        <input value="<?php echo $row->thumb_img_prod;?>" type="hidden" id="hidden">
                                    </div><!--/.col-->
                                </div><!-- /.row -->
                                
                                <div class="form-group">	
                                    <button type="submit" class="btn btn-primary"><?php echo $ButtonSend;?></button>
                                    <button type="reset" class="btn btn-default"><?php echo $ButtonReset;?></button>      
                                </div>
                                
                            </form><!--/.form-->
                        </div><!--/.panel-body-->
                    </div><!--/.panel-->
                </div><!--/.col-->
            </div><!-- /.row -->
        </div><!--/.main-->
        <script src="<?php echo base_url();?>/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/js/chart.min.js"></script>
        <script src="<?php echo base_url();?>/js/easypiechart.js"></script>
        <script src="<?php echo base_url();?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url();?>/js/lumino.glyphs.js"></script>

        <script>

            !function ($) {
                $(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
                    $(this).find('em:first').toggleClass("glyphicon-minus");	  
                }); 
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
                }(window.jQuery);

                $(window).on('resize', function () {
                if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
                })
                $(window).on('resize', function () {
                if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
            })

            // Afisarea pretului dupa miscarea pe bara price

            $('#r1').on("mousemove", function() {
                $('.output').val(this.value +",00" );
            }).trigger("mousemove");
            $('#r1').on("change", function() {
                $('.output').val(this.value +",00" );
            }).trigger("change");

            // Deblocarea butonului dupa completarea a tuturor campurilor

            $(document).on('blur', 'body', function()  {

                var titleRo = $("#titleRo").val();
                var titleIt = $("#titleIt").val();
                var titleRu = $("#titleRu").val();
                var textRo = $("#textRo").val().trim().length;
                var textIt = $("#textIt").val().trim().length;
                var textRu = $("#textRu").val().trim().length;	
                var price = $("#r1").val();					

                if ( price > 0 && titleRo.length > 0 && titleIt.length > 0 && titleRu.length > 0
                    && textRo > 0 && textIt > 0 && textRu > 0 ) {
                    $("button[type='submit']").prop( "disabled", false );
                } else {
                    $("button[type='submit']").prop( "disabled", true );
                }
            });

            // Display image live la incarcare 

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#uploadImage").change(function(){
                readURL(this);
            });

            // La reset sa revie poza default in preview 

            $(document).on('click', 'button[type="reset"]', function() {
                $default = $('#hidden').val();
                $('#preview').attr('src', '<?php echo base_url();?>'+$default);
            }); 	
        </script>	
    </body>
</html>				
				


