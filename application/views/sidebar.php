<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <ul class="nav menu">
        <li <?php if($current=="charts" ) echo "class='active'";?> >
            <a href="<?php echo base_url();?>site/charts">
                <svg class="glyph stroked line-graph">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-line-graph"></use>
                </svg>
                <?php echo $charts; ?>
            </a>
        </li>
        <li <?php if($current=="products" ) echo "class='active'";?> >
            <a href="<?php echo base_url();?>site/product">
                <svg class="glyph stroked bag">
                    <use xlink:href="#stroked-bag"></use>
                </svg>
                <?php echo $products; ?>
            </a>
        </li>
        <li <?php if($current=="insertProduct" ) echo "class='active'";?> >
            <a href="<?php echo base_url();?>site/addProduct">
                <svg class="glyph stroked pencil">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-pencil"></use>
                </svg>
                <?php echo $insertProduct; ?>
            </a>
        </li>
        <li <?php if($current=="managerCategory" ) echo "class='active'";?> >
            <a href="<?php echo base_url();?>site/managerCategory">
                <svg class="glyph stroked app-window">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-app-window"></use>
                </svg>
                <?php echo $managerCategory; ?>
            </a>
        </li>
        <li <?php if($current=="allOrders" ) echo "class='active'";?> >
            <a href="<?php echo base_url();?>site/allOrders">
                <svg class="glyph stroked clipboard with paper">
                    <use xlink:href="#stroked-clipboard-with-paper"/>
               </svg>
                <?php echo $allOrders; ?>
            </a>
        </li>
    </ul>
</div>
<!--/.sidebar-->