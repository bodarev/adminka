<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>

<body>
    <?php include 'navbar.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active">
                    <?php echo $tab_title; ?>
                </li>
            </ol>
        </div>
        <!--/.row-->

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo $tab_title; ?>
                    </div>
                    <div class="panel-body">
                        <table data-toggle="table" data-url="Product/data1.json" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" class="orders">
                            <thead>
                                <tr>
                                    <th data-field="state" data-checkbox="true">
                                        Item ID
                                    </th>
                                    <th data-field="name" data-sortable="true">
                                        <?php echo $orderName; ?>
                                    </th>
                                    <th data-field="address" data-sortable="true">
                                        <?php echo $orderAddress; ?>
                                    </th>
                                    <th data-field="phone" data-sortable="true">
                                        <?php echo $orderPhone; ?>
                                    </th>
                                    <th data-field="date" data-sortable="true">
                                        <?php echo $orderDate; ?>
                                    </th>
                                    <th data-field="id" data-sortable="true">
                                        <?php echo $orderId; ?>
                                    </th>
                                    <th data-field="status" data-sortable="true">
                                        <?php echo $orderStatus; ?>
                                    </th>
                                    <th data-field="total" data-sortable="true">
                                        <?php echo $orderTotal; ?>
                                    </th>
                                    <th data-field="action" data-sortable="false">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($result as $row ) {
                                    
                                    $total = $row->total;
                                    if ( $row->reception == 1 ) {
                                        $total = $total + $row->delivery;
                                    }

                                    $day = date("j ", $row->date);
                                    $month = date("F", $row->date);
                                    $year = date(" Y", $row->date);
                                    $hour = date(" H:i", $row->date);
                                    $date = $day.${$month}.$year.'<br/>'.$hour;

                                    $status = ${$row->status};
    
                                    // adaugam culorile in dependenta de status
    
									switch($row->status) {
										case "waiting":
											$classStatus = 'label-warning';
                                            $prop = '';
											break;
										case "processed":
											$classStatus = 'label-success';
                                            $prop = 'disabled';
											break;
										case "canceled":
											$classStatus = 'label-danger';
                                            $prop = 'disabled';
                                            break;
									}    
                                        
                                    
                                    ?>
                                <tr>
                                    <td data-field="state" data-checkbox="true">
                                        Item ID
                                    </td>
                                    <td data-field="name" data-sortable="true">
                                        <div class="name">
                                            <a href="<?php echo base_url();?>site/user/<?php echo $row->user; ?>">
                                                <?php echo $row->name; ?>
                                            </a>
                                        </div>
                                    </td>
                                    <td data-field="address" data-sortable="true">
                                        <div class="address">
                                            <?php echo $row->address; ?>
                                        </div>
                                    </td>
                                    <td data-field="phone" data-sortable="true">
                                        <div class="phone">
                                            <?php echo $row->phone; ?>
                                        </div>
                                    </td>
                                    <td data-field="date" data-sortable="true">
                                        <div class="date">
                                            <?php echo $date ?>
                                        </div>
                                    </td>
                                    <td data-field="product" data-sortable="true">
                                        <div class="id">
                                            <?php echo $row->id_order; ?>
                                        </div>
                                    </td>
                                    <td data-field="status" data-sortable="true" class="<?php echo $classStatus; ?>">
                                        <div class="status">
                                            <?php echo $status; ?>
                                        </div>
                                    </td>
                                    <td data-field="total" data-sortable="true">
                                        <div class="total">
                                            <?php echo $total.'&nbsp;'.$currency_mdl; ?>
                                        </div>
                                    </td>
                                    <td data-field="action" data-sortable="false">
                                        <div class="columns btn-group">
                                            <button class="btn btn-success process" type="button" name="process" title="Process" <?php echo $prop;?> >
                                                <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"/></svg>
                                            </button>
                                            <input type="hidden" class="id" value="<? echo $row->id_order; ?>" />
                                            <button class="btn btn-danger cancel" type="button" name="cancel" title="Cancel" <?php echo $prop;?> >
                                                <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"/></svg>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--.row-->

    </div>
    <!--/.main-->


    <script src="<?php echo base_url();?>/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>/js/chart.min.js"></script>
    <script src="<?php echo base_url();?>/js/easypiechart.js"></script>
    <script src="<?php echo base_url();?>/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>/js/bootstrap-table.js"></script>
    <script src="<?php echo base_url();?>/js/lumino.glyphs.js"></script>

    <script>
        $(document).ready(function() {
            $(document).on('click', 'button.cancel', function() {
                valID = $(this).parent("div").find("input[type=hidden]").val();
                divStatus = $(this).parents("tr").find("div.status");
                tdLabel = $(this).parents("tr").find("td.label-warning");
                thisButton = $(this).parents("tr").find("button");
                $.ajax({
                        url: "<?php echo base_url(); ?>site/cancelOrder/>",
                        type: 'POST',
                        data: {
                            valore: valID
                        }
                    })
                    .done(function(result) {
                        $(divStatus).empty();
                        $(divStatus).append(result);
                        $(tdLabel).addClass("label-danger");
                        $(tdLabel).removeClass("label-warning");
                        $(thisButton).prop( "disabled", true );
                    });
            });
        });


        $(document).ready(function() {
            $(document).on('click', 'button.process', function() {
                valID = $(this).parent("div").find("input[type=hidden]").val();
                divStatus = $(this).parents("tr").find("div.status");
                tdLabel = $(this).parents("tr").find("td.label-warning");
                thisButton = $(this).parents("tr").find("button");
                $.ajax({
                        url: "<?php echo base_url(); ?>site/processOrder/>",
                        type: 'POST',
                        data: {
                            valore: valID
                        }
                    })
                    .done(function(result) {
                        $(divStatus).empty();
                        $(divStatus).append(result);
                        $(tdLabel).addClass("label-success");
                        $(tdLabel).removeClass("label-warning");
                        $(thisButton).prop( "disabled", true );
                    });
            });
        });




        ! function($) {
            $(document).on("click", "ul.nav li.parent > a > span.icon", function() {
                $(this).find('em:first').toggleClass("glyphicon-minus");
            });
            $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
        }(window.jQuery);

        $(window).on('resize', function() {
            if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
        })
        $(window).on('resize', function() {
            if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
        })
    </script>

</body>

</html>