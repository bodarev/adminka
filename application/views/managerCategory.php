<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>
    <body>
        <?php include 'navbar.php'; ?>
        <?php include 'sidebar.php'; ?>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active"><?php echo $tab_title; ?></li>
            </ol>
        </div><!--/.row-->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">Group</div>
                    <div class="panel-body">
                        <form role="form" action="<?php echo base_url();?>site/addGroup/" method="post" enctype="multipart/form-data">
                            <?php foreach($group as $rowgroup) { ?>
                            <div>
                                <input type="hidden" class="id" value="<? echo $rowgroup->id; ?>" />
                                <span>
                                    <?php echo "${$rowgroup->name}"; ?>
                                </span>
                                <a href="#" class="deleteGroup">[<?php echo $LinkDelete; ?>]</a>
                            </div>
                            <?php }?>
                            <br/>
                            <input class="form-control" placeholder="Group" value="" name="group" id="group">
                            <br/>
                            <div class="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab1" data-toggle="tab">Română</a></li>
                                    <li><a href="#tab2" data-toggle="tab">Italiano</a></li>
                                    <li><a href="#tab3" data-toggle="tab">Русский</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab1">
                                        <div class="form-group">
                                            <br />
                                            <label for="titleRo"><?php echo $productInsertTitle;?>:</label>
                                            <input class="form-control" placeholder="Title" value="" name="titleRo" id="titleRo">
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab2">
                                        <div class="form-group">
                                            <br />
                                            <label for="titleIt"><?php echo $productInsertTitle;?>:</label>
                                            <input class="form-control" placeholder="Title" value="" name="titleIt" id="titleIt">
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab3">
                                        <div class="form-group">
                                            <br />
                                            <label for="textRu"><?php echo $productInsertTitle;?>:</label>
                                            <input class="form-control" placeholder="Title" value="" name="titleRu" id="titleRu">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary group" disabled><?php echo $ButtonSend;?></button>
                            <button type="reset" class="btn btn-default group"><?php echo $ButtonReset;?></button>
                        </form>
                    </div>
                </div>
            </div><!--/.col-->
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading ">Category</div>
                    <div class="panel-body">

                        <form role="form" action="<?php echo base_url();?>site/addCategory/" method="post" enctype="multipart/form-data">
                            <?php foreach($category as $rowcat) { ?>
                            <div>
                                <input type="hidden" class="id" value="<? echo $rowcat->id; ?>" />
                                <span>
                                                    <?php echo "${$rowcat->name_category}"; ?>
                                                </span>
                                <a href="#" class="deleteCategory">[<?php echo $LinkDelete; ?>]</a>
                            </div>
                            <?php }?>
                            <br/>
                            <input class="form-control" placeholder="Category" value="" name="category" id="category">
                            <br/>
                            <div class="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab4" data-toggle="tab">Română</a></li>
                                    <li><a href="#tab5" data-toggle="tab">Italiano</a></li>
                                    <li><a href="#tab6" data-toggle="tab">Русский</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab4">
                                        <div class="form-group">
                                            <br />
                                            <label for="titlecRo"><?php echo $productInsertTitle;?>:</label>
                                            <input class="form-control" placeholder="Title" value="" name="titlecRo" id="titlecRo">
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab5">
                                        <div class="form-group">
                                            <br />
                                            <label for="titlecIt"><?php echo $productInsertTitle;?>:</label>
                                            <input class="form-control" placeholder="Title" value="" name="titlecIt" id="titlecIt">
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab6">
                                        <div class="form-group">
                                            <br />
                                            <label for="textcRu"><?php echo $productInsertTitle;?>:</label>
                                            <input class="form-control" placeholder="Title" value="" name="titlecRu" id="titlecRu">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <label for="category"><?php echo $productSelectCategory;?>:</label>
                            <select class="form-control" id="sel" name="group">
                                            <?php foreach($group as $rowgroup) { ?>         
                                                <option value="<? echo $rowgroup->id; ?>"><?php echo "${$rowgroup->name}"; ?></option>
                                            <?php }?>
                                        </select>
                            <br />
                            <button type="submit" class="btn btn-primary category" disabled><?php echo $ButtonSend;?></button>
                            <button type="reset" class="btn btn-default category"><?php echo $ButtonReset;?></button>
                        </form>
                    </div>
                </div>
            </div><!--/.col-->
        </div><!-- /.row -->
        </div><!--/.main-->
        <script src="<?php echo base_url();?>/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/js/chart.min.js"></script>
        <script src="<?php echo base_url();?>/js/chart-data.js"></script>
        <script src="<?php echo base_url();?>/js/easypiechart.js"></script>
        <script src="<?php echo base_url();?>/js/easypiechart-data.js"></script>
        <script src="<?php echo base_url();?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url();?>/js/lumino.glyphs.js"></script>

        <script>
            ! function($) {
                $(document).on("click", "ul.nav li.parent > a > span.icon", function() {
                    $(this).find('em:first').toggleClass("glyphicon-minus");
                });
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
            }(window.jQuery);

            $(window).on('resize', function() {
                if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
            })
            $(window).on('resize', function() {
                if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
            })

            // Deblocarea butonului dupa completarea a tuturor campurilor

            $(document).on('keyup', 'body', function() {

                var titleRo = $("#titleRo").val();
                var titleIt = $("#titleIt").val();
                var titleRu = $("#titleRu").val();
                var group = $("#group").val();

                if (titleRo.length > 0 && titleIt.length > 0 && titleRu.length > 0 && group.length > 0) {
                    $("button[type='submit'].group").prop("disabled", false);
                } else {
                    $("button[type='submit'].group").prop("disabled", true);
                }
            });

            $(document).on('keyup', 'body', function() {

                var titleRo = $("#titlecRo").val();
                var titleIt = $("#titlecIt").val();
                var titleRu = $("#titlecRu").val();
                var category = $("#category").val();

                if (titleRo.length > 0 && titleIt.length > 0 && titleRu.length > 0 && category.length > 0) {
                    $("button[type='submit'].category").prop("disabled", false);
                } else {
                    $("button[type='submit'].category").prop("disabled", true);
                }
            });

            $(document).on("click", ".deleteGroup", function() {
                location.href = "<?php echo base_url(); ?>site/deleteGroup/" + $(this).siblings("input.id").val();
            });

            $(document).on("click", ".deleteCategory", function() {
                location.href = "<?php echo base_url(); ?>site/deleteCategory/" + $(this).siblings("input.id").val();
            });

            $(document).on("click", "button[type='reset'].group", function() {
                $("button[type='submit'].group").prop("disabled", true);
            });

            $(document).on("click", "button[type='reset'].category", function() {
                $("button[type='submit'].category").prop("disabled", true);
            });
        </script>
    </body>
</html>