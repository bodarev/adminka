<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> <?php echo $tab_title; ?> :: Basic Site</title>

    <link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/bootstrap-table.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/styles.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/my-style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/range.css" rel="stylesheet">
</head>
