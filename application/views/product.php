<!DOCTYPE html>
<html>
    <?php include 'head.php'; ?>

    <body>
        <?php include 'navbar.php'; ?>
        <?php include 'sidebar.php'; ?>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="#"><svg class="glyph stroked home"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-home"></use></svg></a></li>
                    <li class="active">
                        <?php echo $tab_title; ?>
                    </li>
                </ol>
            </div><!--/.row-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                       <div class="panel-heading"><?php echo $tab_title; ?></div>
                        <div class="panel-body">
                            <table data-toggle="table" data-url="Product/data1.json" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" class="products">
                                <thead>
                                    <tr>
                                        <th data-field="state" data-checkbox="true">Item ID</th>
                                        <th data-field="photo" data-sortable="false">
                                            <?php echo $productPhoto; ?>
                                        </th>
                                        <th data-field="product" data-sortable="true">
                                            <?php echo $productName; ?>
                                        </th>
                                        <th data-field="price" data-sortable="true">
                                            <?php echo $productPrice; ?>
                                        </th>
                                        <th data-field="action" data-sortable="false">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($result as $row ) { ?>
                                    <tr>
                                        <td data-field="state" data-checkbox="true">Item ID</td>
                                        <td data-field="photo" data-sortable="false">
                                            <img src="<?php echo base_url().$row->thumb_img_prod; ?>" width="128" />
                                        </td>
                                        <td data-field="product" data-sortable="true">
                                            <h4 class="title_prod"><?php echo $row->title_prod; ?></h4>
                                            <div class="desc_prod">
                                                <?php echo $row->desc_prod; ?>
                                            </div>
                                        </td>
                                        <td data-field="price" data-sortable="true">
                                            <div class="price">
                                                <? echo $row->pret_prod;?> &nbsp;
                                                    <? echo $currency_mdl;?>
                                            </div>
                                        </td>
                                        <td data-field="action" data-sortable="false" class="action">

                                            <div class="columns btn-group pull-right">
                                                <button class="btn btn-info insert" data-toggle="modal" data-target=".bs-example-modal-lg"> 
                                                            <svg class="glyph stroked eye"><use xlink:href="#stroked-eye"/></svg>
                                                        </button>
                                                <button class="btn btn-warning edit" type="button" name="edit" title="Edit">
                                                            <svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"/></svg>
                                                        </button>
                                                <input type="hidden" class="id" value="<? echo $row->id_prod; ?>" />
                                                <button class="btn btn-danger delete" type="button" name="delete" title="Delete">
                                                            <svg class="glyph stroked trash"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-trash"></use></svg>
                                                        </button>

                                            </div>
                                        </td>
                                    </tr>
                                    <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!--/.row-->
        </div><!--/.main-->

        <script src="<?php echo base_url();?>/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>/js/chart.min.js"></script>
        <script src="<?php echo base_url();?>/js/easypiechart.js"></script>
        <script src="<?php echo base_url();?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url();?>/js/bootstrap-table.js"></script>
        <script src="<?php echo base_url();?>/js/lumino.glyphs.js"></script>
        <script>
            $(document).on("click", ".edit", function() {
                location.href = "<?php echo base_url(); ?>site/editProduct/" + $(this).siblings("input.id").val();
            });
            $(document).on("click", ".delete", function() {
                location.href = "<?php echo base_url(); ?>site/deleteProduct/" + $(this).siblings("input.id").val();
            });

            ! function($) {
                $(document).on("click", "ul.nav li.parent > a > span.icon", function() {
                    $(this).find('em:first').toggleClass("glyphicon-minus");
                });
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
            }(window.jQuery);

            $(window).on('resize', function() {
                if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
            })
            $(window).on('resize', function() {
                if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
            })
        </script>
    </body>
</html>


<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
        <div class="modal-body"></div>
    </div>
</div>