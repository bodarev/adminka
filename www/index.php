  <html>  
	<head>

   <script src="/js/jquery.min.js"></script>
<script src="/js/jquery.Jcrop.js"></script>
<script src="/js/jquery.color.js"></script>
<script type="text/javascript">
  jQuery(function($){
    $('#preview').Jcrop({
      // start off with jcrop-light class
      bgOpacity: 0.5,
      bgColor: 'white',
      addClass: 'jcrop-light',
	  aspectRatio: 16 / 10
    });
  });
</script>
<link rel="stylesheet" href="demo_files/main.css" type="text/css" />
<link rel="stylesheet" href="demo_files/demos.css" type="text/css" />
<link rel="stylesheet" href="/css/jquery.Jcrop.css" type="text/css" />

</head>
	
<body>	
	<form>
        <input type='file' id="image" />
		<img src="demo_files/sago.jpg" id="preview" alt="[Jcrop Example]" />
    </form>
	
	
		<script type="text/javascript">
	
		
				function readURL(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$('img').attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				}
			}
    
			$("#image").change(function(){
				readURL(this);
			});
		</script>	
</body>
</html>	